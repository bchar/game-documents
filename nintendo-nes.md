# Nintendo Entertainment System

* 1942
* 1943: The Battle of Midway
* 3-D WorldRunner
* Abadox
* Advanced Dungeons & Dragons: DragonStrike
* Advanced Dungeons & Dragons: Heroes of the Lance
* Advanced Dungeons & Dragons: Hillsfar
* Advanced Dungeons & Dragons: Pool of Radiance
* Adventure Island
* Adventure Island II
* Adventure Island 3
* The Adventures of Bayou Billy
* Adventures of Lolo
* Adventures of Lolo 2
* Adventures of Lolo 3
* Adventures of Rad Gravity
* Air Fortress
* Alpha Mission
* Archon
* Astyanax
* The Bard's Tale: Tales of the Unknown
* The Bard's Tale II: The Destiny Knight
* Base Wars
* Baseball Stars
* Batman
* Batman: Return of the Joker
* Battle City
* The Battle of Olympus
* Battletoads
* Battletoads & Double Dragon
* Binary Land
* Bionic Command
* Blades of Steel
* Blast Master
* Bomberman
* Bomberman II
* Bonk's Adventure
* Boulder Dash
* A Boy and His Blob: Trouble on Blobolonia
* Bubble Bobble
* Bubble Bobble Part 2
* Bucky O'Hare
* Burai Fighter
* BurgerTime
* Captain Skyhawk
* Castlevania
* Castlevania II: Simon's Quest
* Castlevania III: Dracula's Curse
* Chip 'n Dale: Rescue Rangers
* Chip 'n Dale: Rescue Rangers 2
* Choujikuu Yousai Macross
* Clash at Demonhead
* Cobra Triangle
* Contra
* Contra Force
* Cosmic Epsilon
* Crisis Force
* Crystalis
* Darkwing Duck
* Defender II
* Deja Vu
* Demon Sword
* Donkey Kong
* Double Dragon
* Double Dragon II: The Revenge
* Double Dragon III: The Sacred Stones
* Double Dribble
* Dr. Chaos
* Dr. Mario
* // Dragon Warrior
* Dragon's Lair
* DuckTales
* DuckTales 2
* Elevator Action
* Excitebike
* Fantasy Zone
* Faria: A World of Mystery and Danger
* Faxanadu
* Fester's Quest
* // Final Fantasy
* Fire 'n Ice
* G.I. Joe: A Real American Hero
* G.I. Joe: The Atlantis Factor
* Galaga
* Galaxy 5000
* Gargoyle's Quest II
* Gauntlet
* Gauntlet II
* Ghosts'n Goblins
* Golgo 13: Top Secret Episode
* Gradius
* Gradius II
* The Guardian Legend
* Gun-Nac
* Gyruss
* Hammerin' Harry
* Hydlide
* Hydlide 3
* Hydlide Special
* Ice Climber
* Ice Hockey
* Ikari Warriors
* Ikari Warriors II: Victory Road
* Ikari Warriors III: The Rescue
* Image Fight
* The Immortal
* Iron Sword: Wizards & Warriors II
* Jackal
* Jackie Chan's Action Kung Fu
* Journey to Silius
* Joust
* Just Breed
* Kickle Cubicle
* Kid Icarus
* Kid Niki: Radical Ninja
* Kings of the Beach
* Kirby's Adventure
* Kiwi Kraze
* Klax
* Kung Fu
* Kung-Fu Heroes
* Lagrange Point
* Legend of the Ghost Lion
* The Legend of Zelda
* Legendary Wings
* Life Force
* Little Nemo: The Dream Master
* Little Ninja Brothers
* Little Samson
* Lode Runner
* Low G Man: The Low Gravity Man
* Lunar Pool
* M.U.L.E.
* Mach Rider
* The Mafat Conspiracy
* The Magic of Scheherazade
* Magician
* Maniac Mansion
* Mappy-Land
* Marble Madness
* Mario Bros.
* Mega Man
* Mega Man 2
* Mega Man 3
* Mega Man 4
* Mega Man 5
* Mega Man 6
* Mendel Palace
* Metal Gear
* Metal Storm
* Metroid
* Micro Machines
* Mighty Final Fight
* Mike Tyson's Punch-Out!!
* Millipede
* Milon's Secret Castle
* Moon Crystal
* Monster in My Pocket
* Mother (Earthbound)
* Mr. Gimmick!
* Nightshade
* Ninja Gaiden
* Ninja Gaiden II: The Dark Sword of Chaos
* Ninja Gaiden III: The Ancient Ship of Doom
* Nintendo World Cup
* North & South
* Over Horizon
* Palamedes
* Panic Restaurant
* Paperboy
* Paperboy 2
* Parodius Da!
* Phantom Fighter
* Pipe Dream
* Pirates!
* Power Blade
* Power Blade 2
* Prince of Persia
* Princess Tomato in the Salad Kingdom
* Pro Wrestling
* Puzznic
* Qix
* R.C. Pro-Am
* R.C. Pro-Am II
* Rad Racer
* Rad Racer II
* Radia Senki: Reimeihen
* Rainbow Islands: The Story of Bubble Bobble 2
* Rampage
* Recca - Summer Carnival '92
* Renegade
* Ring King
* River City Ransom
* RoadBlasters
* Rolling Thunder
* Rush'n Attack
* Rygar
* S.C.A.T.: Special Cybernetic Attack Team
* Section Z
* Shadow of the Ninja
* Shatterhand
* Shadowgate
* Silkworm
* Silver Surfer
* Skate of Die!
* Skate or Die 2: The Search for Double Trouble
* Sky Shark
* Slalom
* Smash TV
* Snake Rattle 'n' Roll
* Snow Brothers
* Solar Jetman: Hunt for the Golden Warpship
* Solomon's Key
* Solstice: The Quest for the Staff of Demnos
* Spy Hunter
* Sqoon
* Star Soldier
* Starship Hector
* StarTropics
* Stinger
* Street Fighter 2010: The Final Fight
* Strider
* Super C
* Super Dodge Ball
* Super Mario Bros.
* Super Mario Bros. 2
* Super Mario Bros. 3
* Super Spy Hunter
* Super Turrican
* Super Xevious: GAMP no Nazo
* Sword Master
* Tecmo Bowl
* Tecmo Super Bowl
* Tecmo World Wrestling
* Teenage Mutant Ninja Turtles
* Teenage Mutant Ninja Turtles II: The Arcade Game
* Teenage Mutant Ninja Turtles III: The Manhattan Project
* Teenage Mutant Ninja Turtles: Tournament Fighters
* Tetris
* Tetris 2
* Tiger Heli
* Tombs & Treasure
* Toobin'
* Totally Rad
* Town & Country Surf Designs: Wood & Water Rage
* Town & Country II: Thrilla's Surfari
* Trog!
* Trojan
* Twinbee
* Twinbee 3
* Ufouria: The Saga
* The Ultimate Stuntman
* Ultima III: Exodus
* Ultima IV: Quest of the Avatar
* Ultima V: Warriors of Destiny
* Uninvited
* Vice: Project Doom
* Volguard II
* Wario's Woods
* Willow
* Wizardry: Proving Grounds of the Mad Overlord
* Wizardry II: The Knight of Diamonds
* Wizards & Warriors
* Wizards & Warriors III: Kuros: Visions of Power
* Wrath of the Black Manta
* Wurm: Journey to the Center of the Earth
* Xevious
* Xexyz
* Yoshi
* Yoshi's Cookie
* // Ys
* Zanac
* Zelda II: The Adventure of Link
* Zombie Nation
