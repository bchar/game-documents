# Super Nintendo Entertainment System

* ActRaiser
* Aladdin
* Alien 3
* Axelay
* Blackthorne
* Breath of Fire
* Bust-a-Move
* Chrono Trigger
* Contra III
* Demon's Crest
* Donkey Kong Country
* EarthBound
* Earthworm Jim
* EVO: Search for Eden
* F-Zero
* Fatal Fury 2
* Final Fantasy
* Final Fight
* Flashback
* Gradius III
* Harvest Moon
* Illusion of Gaia
* International Superstar Soccer
* Joe & Mac 2: Lost in the Tropics
* Jungle Strike
* Ken Griffey Jr.'s Winning Run
* Killer Instinct
* Kirby's Avalance
* Kirby's Dream Course
* Kirby's Dream Land 3
* Kirby Super Star
* Legend of the Mystical Ninja
* Lufia II: Rise of the Sinistrals
* Madden '94
* Mario Paint
* Mega Man X, X2, X3
* Metal Warriors
* Mortal Kombat II
* NBA Jam
* NHL '94
* Ogre Battle: March of the Black Queen
* Out of this World
* Pilotwings
* Pocky & Rocky 2
* Populous
* Rock n' Roll Racing
* Secret of Mana
* Shadowrun
* Sim City
* Soul Blazer
* Sparkster
* Spider-Man & Venom: Maximum Carnage
* Star Fox
* Street Fighter Alpha 2
* Street Fighter II
* Turbo
* Stunt Race FX
* Sunset Riders
* Super Bomberman, 2
* Super Castlevania IV
* Super Double Dragon
* Super Ghouls 'n Ghosts
* Super Mario All-Stars
* Super Mario Kart
* Super Mario RPG: Legend of the Seven Stars
* Super Mario World
* Super Mario World 2: Yoshi's Island
* Super Metroid
* Super Punch-Out!!
* Super Star Wars, Return of the Jedi, Empire Strikes Back
* Super Tennis
* Super Turrican 2
* Teenage Mutant Ninja Turtles: Turtles in Time
* Tetris & Dr. Mario
* Tetris Attack
* The Adventures of Batman & Robin
* The Death and Return of Superman
* The Legend of Zelda: A Link to the Past
* The Lion King
* The Lost Vikings
* The Magical Quest Starring Mickey Mouse
* Tiny Toon Adventures: Buster Busts Loose!
* Top Gear 2
* Ultimate Mortal Kombat 3
* Uniracers
* UN Squadron
* Wario's Woods
* X-Men: Mutant Apocalypse
* Yoshi's Cookie
* Zombies Ate My Neighbors
