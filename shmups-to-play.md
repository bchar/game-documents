# Sega Saturn
* DonPachi
* DoDonPachi
* Battle Garegga
* Batsugun
* Layer Section & II
* Souk Yugurentai
* Twinkle Star Sprites
* Strikers 1945 & II
* Gunbird
* Sengoku Blade
* Cotton 2
* Cotton Boomerang
* Parodius
* Gradius Deluxe Pack
* Detana Twinbee Yahho! Deluxe Pack
* Salamander Deluxe Pack Plus
* Kingdom Grand Prix (Shippu Mahou Daisakusen)
* Shienryu
* Sonic Wings Special
* Image Fight
* X-Multiply
* Guardian Force
* Gun Frontier
* Metal Black
* 3 Wonders
* Darius 2
* Darius Gaiden
* Fantasy Zone
* Gekirindan
* Thunder Force Gold Pack 1 & 2
* In the Hunt
* Skull Fang
* Kyukyoku Tiger II Plus
* Capcom Generation 1, 3, 4
* Dezaemon 2
* Sol Divide
* Konami Antiques MSX Collection
* Space Invaders
* Cho Aniki
* Steam Heart's
* Macross: Do You Remember Love?
* Terra Cresta 3D
* Tukai! Slot Shooting
* Planet Joker

# Windows
* The Embodiment of Scarlet Devil
* Cho Ren Sha 68K
* Warning Forever
* Hydorah
* They Came From Verminest!
* Stargunner
* Tyrian 2000